# https://github.com/operator-framework/operator-sdk
# https://devops.college/developing-kubernetes-operator-is-now-easy-with-operator-framework-d3194a7428ff

image = registry.gitlab.com/thelazyenginerd/hello-world-operator

build:
	operator-sdk build $(image)

push:
	docker push $(image)

rm:
	docker rmi -f $(image)

create:
	kubectl create -f deploy/rbac.yaml
	kubectl create -f deploy/crd.yaml
	kubectl create -f deploy/operator.yaml

delete:
	kubectl delete -f deploy/operator.yaml
	kubectl delete -f deploy/crd.yaml
	kubectl delete -f deploy/rbac.yaml
